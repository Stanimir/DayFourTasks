import java.util.ArrayList;
import java.util.List;

public class Graph3 {
	static private Node[] nodes;
	
	public Graph3(Graph2 graph2) {
		parser(graph2);
	}

	public Node[] getNodes() {
		return nodes;
	}


	public void setNodes(Node[] nodes) {
		Graph3.nodes = nodes;
	}
	
	public int getIndex(Vertex vertex)
	{
		
		for(int i = 0;i<nodes.length;i++)
		{
			
			if(nodes[i].getVertex().equals(vertex))
				
				return i;
			
		}
		
		return -1;
	}

	public void parser(Graph2 graph) {
		List<Vertex> vertexs = graph.getVertexes();
		nodes = new Node[vertexs.size()];
		ArrayList<MyEdge> edges;
		
		for (int i = 0;i<graph.getVertexes().size();i++) {
			nodes[i] = new Node(vertexs.get(i));
		}
		
		for (int i = 0; i < vertexs.size(); i++) {
			edges = new ArrayList<>();
			
			for(Edge edge : graph.getEdges())
			{
				
				if(edge.getDestination() == vertexs.get(i))
				{
					edges.add(new MyEdge(nodes[getIndex(edge.getSource())], edge.getWeight(), edge.getPrice()));
				} else if (edge.getSource() == vertexs.get(i)) {
					edges.add(new MyEdge(nodes[getIndex(edge.getDestination())], edge.getWeight(), edge.getPrice()));
				}
					
			}
			
			nodes[i].setEdges(edges);
		}
		
	}
}
