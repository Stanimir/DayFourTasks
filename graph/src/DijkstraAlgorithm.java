import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Predicate;

public class DijkstraAlgorithm {
	private Graph3 graph;
	private static Node[] nodes;
	private ArrayList<Node> visited;
	private Queue<Node> updateQueue = new LinkedList<>();
	private Vertex end;
	private ArrayList<Vertex> path;
	private Method[] methods = new Method[3];
	private Method[] methodsSecondary = new Method[3];
	private String[] labels = {"weight: ", "price: "};

	public DijkstraAlgorithm() throws NoSuchMethodException, SecurityException {
		methods[0] = Node.class.getMethod("getWeight");
		methods[1] = Node.class.getMethod("setWeight", int.class);
		methods[2] = MyEdge.class.getMethod("getWeight");
		methodsSecondary[0] = Node.class.getMethod("getPrice");
		methodsSecondary[1] = Node.class.getMethod("setPrice", int.class);
		methodsSecondary[2] = MyEdge.class.getMethod("getPrice");
	}

	public void sortByWeight(Graph2 graph2, Vertex start, Vertex end, String typePriorityName) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		if (typePriorityName.equals("Price")) {
			Method[] swap = methods;
			methods = methodsSecondary;
			methodsSecondary = swap;
			String swapS = labels[0];
			labels[0] = labels[1];
			labels[1] = swapS;
		}

		path = new ArrayList<>();
		visited = new ArrayList<>();
		this.end = end;
		graph = new Graph3(graph2);
		nodes = graph.getNodes();
		Queue<Node> que = new LinkedList<>();
		Node current = nodes[graph.getIndex(start)];
		methods[1].invoke(current, 0);
		methodsSecondary[1].invoke(current, 0);
		que.add(current);
		visited.add(current);
		
		while (!que.isEmpty()) {
			current = que.remove();
			
			if (current.getVertex().equals(end)) {
				continue;
				
			} else {
				
				for (MyEdge edge : current.getEdges()) {
					
					if (!visited.contains(edge.getNextNode())) {
						que.add(edge.getNextNode());
						visited.add(edge.getNextNode());
						methodsSecondary[1].invoke(edge.getNextNode(),((int) methodsSecondary[0].invoke(current) + (int) methodsSecondary[2].invoke(edge)));
						methods[1].invoke(edge.getNextNode(), ((int) methods[0].invoke(current) + (int) methods[2].invoke(edge)));

					} else {
						
						if ((int) methods[0].invoke(edge.getNextNode()) > (int) methods[0].invoke(current) + (int) methods[2].invoke(edge)) {
							methods[1].invoke(edge.getNextNode(),((int) methods[0].invoke(current) + (int) methods[2].invoke(edge)));
							methodsSecondary[1].invoke(edge.getNextNode(), ((int) methodsSecondary[0].invoke(current) + (int) methodsSecondary[2].invoke(edge)));
							updateNodes(edge.getNextNode());
						}
						
					}
					
				}
				
			}
	
		}
		
		current = nodes[graph.getIndex(end)];
		Node min;
		while (!current.getVertex().equals(start)) {
			min = current.getEdges().get(0).getNextNode();

			for (MyEdge edge : current.getEdges()) {

				if ((int) methods[0].invoke(min) >(int) methods[0].invoke( edge.getNextNode()) + (int) methods[2].invoke(edge)) {
					min = edge.getNextNode();
				}

			}
			
			current = min;
			path.add(0, current.getVertex());
		}
		path.add(nodes[graph.getIndex(end)].getVertex());
		System.out.println(labels[0] + (int) methods[0].invoke(nodes[graph.getIndex(end)]));
		System.out.println(labels[1] + (int) methodsSecondary[0].invoke(nodes[graph.getIndex(end)]));
		
		System.out.println(path);
	}

	private void updateNodes(Node node) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		updateQueue.add(node);
		Node current;
		
		while (!updateQueue.isEmpty()) {
			current = updateQueue.remove();
			
			for (MyEdge edge : current.getEdges()) {
				
				if (visited.contains(edge.getNextNode())) {
					
					if ((int) methods[0].invoke(edge.getNextNode()) >(int) methods[0].invoke( current) + (int) methods[2].invoke(edge)) {
						methods[1].invoke(edge.getNextNode(), ((int) methods[0].invoke(current) + (int) methods[2].invoke(edge)));
						methodsSecondary[1].invoke(edge.getNextNode(), ((int) methodsSecondary[0].invoke(current) + (int) methodsSecondary[2].invoke(edge)));
						
						if (!edge.getNextNode().getVertex().equals(end))
							updateQueue.add(edge.getNextNode());
					}
					
				}
				
			}
			
		}
		
	}

	public ArrayList<Vertex> getPath() {
		return path;
	}

}
