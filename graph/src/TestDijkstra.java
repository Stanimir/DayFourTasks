import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class TestDijkstra {

	private List<Vertex> nodes;
	private List<Edge> edges;

	public void testExcute() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		nodes = new ArrayList<Vertex>();
		edges = new ArrayList<Edge>();
		for (int i = 0; i < 11; i++) {
			Vertex location = new Vertex("Node_" + i, "Node_" + i);
			nodes.add(location);
		}

		addLane("Edge_0", 0, 1, 85, 50);
		addLane("Edge_1", 0, 2, 217, 45);
		addLane("Edge_2", 0, 4, 173, 100);
		addLane("Edge_3", 2, 6, 186, 15);
		addLane("Edge_4", 2, 7, 103, 143);
		addLane("Edge_5", 3, 7, 183, 18);
		addLane("Edge_6", 5, 8, 250, 65);
		addLane("Edge_7", 8, 9, 84, 97);
		addLane("Edge_8", 7, 9, 167, 43);
		addLane("Edge_9", 4, 9, 502, 15);
		addLane("Edge_10", 9, 10, 40, 12);
		addLane("Edge_11", 1, 10, 600, 33);

		// Lets check from location Loc_1 to Loc_10
		Graph2 graph = new Graph2(nodes, edges);
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm();
		//Put Price for price prioritized sort and anything else for Weight prioritized sort.
		dijkstra.sortByWeight(graph, nodes.get(0), nodes.get(10), "Weight");
		ArrayList<Vertex> path = dijkstra.getPath();

		for (Vertex vertex : path) {
			System.out.println(vertex);
		}

	}

	private void addLane(String laneId, int sourceLocNo, int destLocNo, int duration, int price) {
		Edge lane = new Edge(laneId, nodes.get(sourceLocNo), nodes.get(destLocNo), duration, price);
		edges.add(lane);
	}
}