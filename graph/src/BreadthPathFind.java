import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class BreadthPathFind {
	private boolean[] vis;

	private HashMap<Integer, Integer> prev = new HashMap<Integer, Integer>();

	public ArrayList<Integer> getDirections(Graph graph, Integer start, Integer finish) {
		vis = new boolean[graph.V()];
		ArrayList<Integer> directions = new ArrayList<>();
		Queue<Integer> q = new LinkedList<>();
		Integer current = start;
		boolean[][] adj = graph.getAdjacencyMatrix();
		ArrayList<Integer> connectedNodes = new ArrayList<>();
		q.add(current);
		vis[current] = true;
		while (!q.isEmpty()) {
			current = q.remove();
			if (current.equals(finish)) {
				break;
			} else {
				for (int i = 0; i < graph.V(); i++) {
					if (adj[current][i])
						connectedNodes.add(i);
				}
				for (Integer node : connectedNodes) {
					if (!vis[node]) {
						q.add(node);
						vis[node] = true;
						prev.put(node, current);
					}
				}
			}
		}
		if (!current.equals(finish)) {
			System.out.println("can't reach destination");
		}
		for (Integer node = finish; node != null; node = prev.get(node)) {

			directions.add(0, node);
		}

		return directions;
	}
}
