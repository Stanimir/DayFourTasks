
public class MyEdge {
	private Node nextNode;
	private final int weight;
	private final int price;

	public MyEdge(Node nextNode, int weight, int price) {
		super();
		this.nextNode = nextNode;
		this.weight = weight;
		this.price = price;
	}

	public Node getNextNode() {
		return nextNode;
	}

	public void setNextNode(Node nextNode) {
		this.nextNode = nextNode;
	}

	public int getWeight() {
		return weight;
	}

	public int getPrice() {
		return price;
	}
}
