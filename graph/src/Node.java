import java.util.ArrayList;

public class Node {
	private Vertex vertex;
	private ArrayList<MyEdge> edges;
	private int weight;
	private int price;

	public Node(Vertex vertex) {
		super();
		this.vertex = vertex;
		edges = new ArrayList<>();
		this.weight = Integer.MAX_VALUE;
		this.price = Integer.MAX_VALUE;
	}

	public Vertex getVertex() {
		return vertex;
	}

	public void setVertex(Vertex vertex) {
		this.vertex = vertex;
	}

	public ArrayList<MyEdge> getEdges() {
		return edges;
	}

	public void setEdges(ArrayList<MyEdge> edges) {
		this.edges = edges;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
