import java.lang.reflect.InvocationTargetException;
import java.util.Stack;

public class Main {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Graph graph = new Graph(10);
		graph.addEdge(0, 1);
		graph.addEdge(0, 2);
		graph.addEdge(0, 4);
		graph.addEdge(1, 2);
		graph.addEdge(3, 4);
		graph.addEdge(3, 7);
		graph.addEdge(3, 6);
		graph.addEdge(5, 6);
		graph.addEdge(7, 5);
		graph.addEdge(1, 8);
		graph.addEdge(8, 3);
		graph.addEdge(2, 9);
		graph.addEdge(9, 3);
		graph.addEdge(4, 6);

//		Graph graph = new Graph(7);
//		graph.addEdge(0, 1);
//		graph.addEdge(0, 2);
//		graph.addEdge(0, 4);
//		graph.addEdge(1, 2);
//		graph.addEdge(1, 3);
//		graph.addEdge(2, 3);
//		graph.addEdge(3, 4);
//		graph.addEdge(3, 5);
//		graph.addEdge(3, 6);
//		graph.addEdge(4, 6);
//		graph.addEdge(5, 6);

		System.out.println(graph.toString());
		Stack<Integer> path = new DepthFirstTraversal().depthSerachPath(graph, 0, 6);

		new TestDijkstra().testExcute();
		BreadthPathFind breadth = new BreadthPathFind();
		System.out.println("breath search: " + breadth.getDirections(graph, 0, 5));
		System.out.println("depth search: " + path);

	}

}
