import java.util.Stack;

public class DepthFirstTraversal {
	static Stack<Integer> stack = new Stack<>();
	boolean[][] adj;
	boolean visited[];
	int limit;
	
	public Stack<Integer> depthSerachPath(Graph graph, int start, int end) {
		boolean[][] adj = graph.getAdjacencyMatrix();
		boolean[] visited = new boolean[graph.V()];
		Stack<Integer> path = new Stack<>();
		path.push(start);
		int current = start;
		int nextConnection = -1;
		visited[start] = true;
		
		while (current != end && path.size() != 0) {
			nextConnection = -1;

			for (int i = 0; i < graph.V(); i++) {

				if (adj[current][i] && !visited[i]) {
					nextConnection = i;
					adj[current][i] = false;
					break;
				}

			}
			
			if (nextConnection > 0) {
				path.add(nextConnection);
				current = nextConnection;
				visited[nextConnection] = true;
			} else {
				path.pop();
				
				if (!path.isEmpty())
					current = path.peek();
				
			}

		}

		return path;
	}
}
